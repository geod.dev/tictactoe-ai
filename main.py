from ai import ai_turn, evaluate


def player_turn():
    print_plate(game)
    while True:
        place = int(input("Enter a case (1-9): ")) - 1

        if game[place] is None:
            game[place] = 1
            break

        print("That place is already taken.")
    print_plate(game)


def print_plate(plate):
    printable = [" " if x is None else "X" if x == 1 else "O" for x in plate]
    print(
        "\n|" + "|".join(printable[0:3]) + "|\n|" + "|".join(printable[3:6]) + "|\n|" + "|".join(printable[6:9]) + "|\n"
    )


if __name__ == '__main__':
    game = [None, None, None, None, None, None, None, None, None]
    names = ["AI", "Player"]
    turn = 0
    winner = None

    while None in game:
        if turn == 0:
            game = ai_turn(game)
        else:
            player_turn()

        if evaluate(game):
            winner = turn
            if turn == 0:
                print_plate(game)
            break

        turn = 1 - turn

    print("Game Over !")
    if winner is None:
        print("Tie !")
    else:
        print(names[winner], "wins !")
