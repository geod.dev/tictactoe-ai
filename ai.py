winning_combinations = [[(3, 4, 5), (1, 4, 7), (0, 4, 8), (2, 4, 6)], [(0, 1, 2), (0, 3, 6)], [(2, 5, 8), (6, 7, 8)]]


def evaluate(plate):
    if any(plate[a] == plate[b] == plate[c] is not None for a, b, c in winning_combinations[0]):
        return 10 if plate[4] == 0 else -10
    elif any(plate[a] == plate[b] == plate[c] is not None for a, b, c in winning_combinations[1]):
        return 10 if plate[0] == 0 else -10
    elif any(plate[a] == plate[b] == plate[c] is not None for a, b, c in winning_combinations[2]):
        return 10 if plate[8] == 0 else -10
    return 0


def ai_turn(game):
    plates = get_next_plates(game, 0)
    score = -100
    for p in plates:
        s = minimax(p, 1, 1)
        if score < s:
            score = s
            game = p
    return game


def get_next_plates(game, player):
    plates = []
    for c in range(len(game)):
        if game[c] is None:
            np = game.copy()
            np[c] = player
            plates.append(np)
    return plates


def minimax(plate, player, depth):
    score = evaluate(plate)
    if (not None in plate) or score:
        return score + depth

    if player == 0:
        best = -100
        for p in get_next_plates(plate, player):
            best = max(best, minimax(p, 1, depth + 1))
        return best

    else:
        best = 100
        for p in get_next_plates(plate, player):
            best = min(best, minimax(p, 0, depth + 1))
        return best
